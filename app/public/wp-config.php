<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HBWxf7meEjtzzFAR6MLlsmeS05Lmnq97GcptiJCHUNyeUA7FJ+HDn279s0+gIsOcYI+gP2FeD8ByO+BQKohe5g==');
define('SECURE_AUTH_KEY',  'eNsvyt+qSPXKUqbg7hJ2tt8Wxms//8Q1Z7NE1R/k+LtHzYTiQABJN0wUpE1d/yh2d6e3d7Dknt9RrsdIornyCQ==');
define('LOGGED_IN_KEY',    'yG8YjVwZbXq44vS5uVVEIDx1Vmuqn4jSd+YruWpGnpQZGkIEatXQlrIXBT/0XXURNoNPbIOx2LAjcgkxFTcMOg==');
define('NONCE_KEY',        'aR5HPPcVldYBcbb6WljEPwZ/Cdv9XuZVLF9MqrXZ2KOk8ny6cfmvjxHBU7NAOcSgNjjQn+knDoICsLp+Su1Jqw==');
define('AUTH_SALT',        'sgWEa5JQXOJs+mmXXxsXERGC9cT5oulHR4YhZPhBMFiSjxh1zQMEKuTveK2Gvyry54RJBfvd/Yk9SgzHUespOQ==');
define('SECURE_AUTH_SALT', 'u3AQJ05M0/ys8/dDAM6pHiEfm+qz7HGQQEgZk6wYwhwCVgECB5Rh7BiDDUS6NbZgKaH053foppumBv3MbJntlw==');
define('LOGGED_IN_SALT',   '4y6wwkdUNwVGVGCGjacSBobtFfAqz+yZYUADlDP4WXMsxko4p9f0onKU5CqHXdsyFVPGFzjrGwy92GcBfM8qkA==');
define('NONCE_SALT',       'x0cm2e2AdiQ2TLm7hH5EoJyMIVANoNUjipew65xQW1wh5NeeFMm5uyVg1GNn41KF/CduGxHH5dW93KV3ImXs8A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
