<?php get_header();?>


<!-----Home section Starts-->
<section class="home" id="home">
  <div class="content">
    <h3>EAT MORE</h3>
    <p>Step into Eat More and you're welcomed into an ambience where a balance of modern innovation, elegance and tradition combine the Oriental essence of food, decor and music, all in one space.</p>
    <a href="#order" class="btn">Order Now</a>
  </div>

  <div class="image" id="image">
    <img src="<?php bloginfo('template_directory');?>/images/home-img.png" alt="">
  </div>
</section>


<!---Home section Ends-->

<!-- speciality section starts  -->

<section class="speciality" id="speciality">

  <h1 class="heading"> our <span>speciality</span> </h1>

  <div class="box-container">

      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/<?php bloginfo('template_directory');?>/images/s-img-1.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-1.png" alt="">
              <h3>tasty burger</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>
      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/images/s-img-2.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-2.png" alt="">
              <h3>tasty pizza</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>
      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/images/s-img-3.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-3.png" alt="">
              <h3>cold ice-cream</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>
      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/images/s-img-4.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-4.png" alt="">
              <h3>cold drinks</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>
      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/images/s-img-5.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-5.png" alt="">
              <h3>tasty sweets</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>
      <div class="box">
          <img class="image" src="<?php bloginfo('template_directory');?>/images/s-img-6.jpg" alt="">
          <div class="content">
              <img src="<?php bloginfo('template_directory');?>/images/s-6.png" alt="">
              <h3>healty breakfast</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda inventore neque amet ipsa tenetur voluptates aperiam tempore libero labore aut.</p>
          </div>
      </div>

  </div>

</section>

<!-- speciality section ends -->

<!-- popular section starts  -->

<section class="popular" id="popular">

  <h1 class="heading"> Most <span>Popular</span> Foods </h1>

  <div class="box-container">

      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-1.jpg" alt="">
          <h3>tasty burger</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order Now</a>
      </div>
      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-2.jpg" alt="">
          <h3>tasty cakes</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order now</a>
      </div>
      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-3.jpg" alt="">
          <h3>tasty sweets</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order now</a>
      </div>
      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-4.jpg" alt="">
          <h3>tasty cupcakes</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order now</a>
      </div>
      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-5.jpg" alt="">
          <h3>cold drinks</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order now</a>
      </div>
      <div class="box">
          <span class="price"> $5 - $20 </span>
          <img src="<?php bloginfo('template_directory');?>/images/p-6.jpg" alt="">
          <h3>cold ice-cream</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <a href="#" class="btn">order now</a>
      </div>

  </div>

</section>

<!-- popular section ends -->

<!-- steps section starts  -->

<div class="step-container">

  <h1 class="heading">how it <span>works</span></h1>

  <section class="steps">

      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/step-1.jpg" alt="">
          <h3>choose your favorite food</h3>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/step-2.jpg" alt="">
          <h3>free and fast delivery</h3>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/step-3.jpg" alt="">
          <h3>easy payments methods</h3>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/step-4.jpg" alt="">
          <h3>and finally, enjoy your food</h3>
      </div>
  
  </section>

</div>

<!-- steps section ends -->

<!-- gallery section starts  -->

<section class="gallery" id="gallery">

  <h1 class="heading"> our food <span> gallery </span> </h1>

  <div class="box-container">

      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-1.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-2.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-3.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-4.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">ordern now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-5.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-6.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-7.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-8.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/g-9.jpg" alt="">
          <div class="content">
              <h3>tasty food</h3>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, ipsum.</p>
              <a href="#" class="btn">Order Now</a>
          </div>
      </div>

  </div>

</section>

<!-- gallery section ends -->

<!-- review section starts  -->

<section class="review" id="review">

  <h1 class="heading"> our customers <span>reviews</span> </h1>

  <div class="box-container">

      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/pic1.png" alt="">
          <h3>john deo</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti delectus, ducimus facere quod ratione vel laboriosam? Est, maxime rem. Itaque.</p>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/pic2.png" alt="">
          <h3>john deo</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti delectus, ducimus facere quod ratione vel laboriosam? Est, maxime rem. Itaque.</p>
      </div>
      <div class="box">
          <img src="<?php bloginfo('template_directory');?>/images/pic3.png" alt="">
          <h3>john deo</h3>
          <div class="stars">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
          </div>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti delectus, ducimus facere quod ratione vel laboriosam? Est, maxime rem. Itaque.</p>
      </div>

  </div>

</section>

<!-- review section ends -->

<!-- order section starts  -->

<section class="order" id="order">

  <h1 class="heading"> <span>order</span> now </h1>

  <div class="row">
      
      <div class="image">
          <img src="<?php bloginfo('template_directory');?>/images/order-img.jpg" alt="">
      </div>

      <form action="">

          <div class="inputBox">
              <input type="text" placeholder="name">
              <input type="email" placeholder="email">
          </div>

          <div class="inputBox">
              <input type="number" placeholder="number">
              <input type="text" placeholder="food name">
          </div>

          <textarea placeholder="address" name="" id="" cols="30" rows="10"></textarea>

          <input type="submit" value="order now" class="btn">

      </form>
      

  </div>

</section>

<!-- order section ends -->


<!---Events section starts---->


<section class="event" id="event">

<h1 class="heading"> <span>Upcoming </span> Events </h1>

<div id="primary">

<div id="content" role="main">



<?php

$hotelspost = array( 'post_type' => 'hotel', );
$loop = new WP_Query( $hotelspost );

?>
<?php while ( $loop->have_posts() ) : $loop->the_post();?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-content"><?php the_content(); ?></div>
</article>

<?php endwhile; ?>

</div>

</div>


</section>





<!---Events section ends---->



<script src="js/script.js"></script>
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>



</body>

<?php get_footer();?>
