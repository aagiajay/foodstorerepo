<?php


function load_stylesheets()

{

    wp_register_style('styles', get_template_directory_uri() . '/css/style.css', array(), 1,'all');
    wp_enqueue_style('styles');

   
}

add_action('wp_enqueue_scripts','load_stylesheets');


//Load Scripts

function addjs(){


    wp_register_script('jquery', get_template_directory_uri(). '/js/script.js',array(),1,1,1);
    wp_enqueue_script('jquery');
}

function my_custom_post_hotel() {

    //labels array added inside the function and precedes args array
    
    $labels = array(
    'name' => _x( 'Hotels', 'post type general name' ),
    'singular_name' => _x( 'Hotel', 'post type singular name' ),
    'add_new' => _x( 'Add New', 'Hotel' ),
    'add_new_item' => __( 'Add New Hotel' ),
    'edit_item' => __( 'Edit Hotel' ),
    'new_item' => __( 'New Hotel' ),
    'all_items' => __( 'All Hotels' ),
    'view_item' => __( 'View Hotel' ),
    'search_items' => __( 'Search hotels' ),
    'not_found' => __( 'No hotels found' ),
    'not_found_in_trash' => __( 'No hotels found in the Trash' ),
    'parent_item_colon' => '',
    'menu_name' => 'Hotels'
    );
    
    // args array
    
    $args = array(
    'labels' => $labels,
    'description' => 'Displays city hotels and their ratings',
    'public' => true,
    'menu_position' => 4,
    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive' => true,
    );
    
    register_post_type( 'hotel', $args );
    }
    add_action( 'init', 'my_custom_post_hotel' );

    //registration of taxonomies

function my_taxonomies_hotel() {

    //labels array
    
    $labels = array(
    'name' => _x( 'Hotel Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Hotel Category', 'taxonomy singular name' ),
    'search_items' => __( 'Search Hotel Categories' ),
    'all_items' => __( 'All Hotel Categories' ),
    'parent_item' => __( 'Parent Hotel Category' ),
    'parent_item_colon' => __( 'Parent Hotel Category:' ),
    'edit_item' => __( 'Edit Hotel Category' ),
    'update_item' => __( 'Update Hotel Category' ),
    'add_new_item' => __( 'Add New Hotel Category' ),
    'new_item_name' => __( 'New Hotel Category' ),
    'menu_name' => __( ' Hotel Categories' ),
    );
    
    //args array
    
    $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    );
    
    register_taxonomy( 'hotel_category', 'hotel', $args );
    }
    
    add_action( 'init', 'my_taxonomies_hotel', 0 );




