<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<title>Food Store</title>


<!---font awesome cdn link-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!----Custom css file link-->

<link rel="stylesheet" href="style.css">


<?php wp_head();?>

</head>


<body>

    <!----Header-->

    <header>
  <img src="<?php bloginfo('template_directory');?>/images/logo.png" width="100" height="100" alt="logo" class="logo">
  <div id="menu-bar" class="fas fa-bars"></div>

  <nav class="navbar">
      <a href="#home">home</a>
    <a href="#speciality">speciality</a>
    <a href="#popular">popular</a>
    <a href="#gallery">gallery</a>
    <a href="#review">review</a>
    <a href="#order">order</a>
  </nav>


    </header>

<!-----Header Ends-->
